

#' joint.distrib.2v.LinLog : Return a list of all the candidates matrix and their partition vectors.
#'
#' @param matrix_lin numerical matrix with in columns observations of one statistical individual to categorize.
#' The linear scale will be used for the categorisation of all columns.   
#' @param matrix_log numerical matrix with same number of columns as matrix_lin. Columns are also observations 
#' of one statistical individual to categorize.The log scale will be used in thos case. See the log_breaks function fore more information
#' @param range_lin numerical vector. Contains the numbers of bins related to matrix_lin that will be used for the histogram. 
#' @param range_log numerical vector. Contains the numbers of bins related to matrix_log that will be used for the histogram
#' @param range_alpha_ Contains thevalues that will be used for alpha. See the log_breaks function fore more information.
#' @param round_LogBreaks Integer. Number of decimals that will be used for the breaks created via a logarithmic scale.
#' Exactly the argument digits of the function base::Round.
#' @param N.cores Numbers of cores that will be used. By default, N.cores=detectCores()-1.
#' @param DIGLAB_lin Exactly the argument dig.lab of the base::cut function. DIGLAB_X = 3 by default.
#' @param effect.threshold.begin NA by default. Numeric value between xmin and xmax. If it isn't NA, 
#' the first class is created with xmin and effect.threshold.begin. The same value is used 
#' for all the joint distributions created.
#' @param effect.threshold.end NA by default. Numeric value between xmin and xmax and greater than 
#' effect.threshold.begin. If it isn't NA, the last class is created with xmax and effect.threshold.end.
#' The same value is used for all the joint distributions created.
#' @param centering_status FALSE by default. Same as center argument of base::scale function.
#' Define if columns of joint distributions should be centered or not.
#' @param scaling_status FALSE by default. Same as scale argument of base::scale function.
#' Define if columns of joint distributions should be scaled or not.
#' 
#' @return The joint distributions by the combination of parameters in range_lin, range_log and range_alpha_ 
#' Returns : 
#' \describe{
#' \item{x_appit}{An environnement (list) containing length(range_lin)*length(range_log) joint distributions. 
#' Each joint distribution is a matrix of dimension (n_ind x (nclasses_tpr x nclasses_rdc)). Each value of 
#' the matrix represent  a count number of parameter (time per exemple) that the individual had conditions describe by the bins}
#' \item{range_dframe}{A data frame for which each row is associated to one joint distributions inside  x_appit. range_dframe
#' has five columns. range_lin, range_log and range_alpha_ are same as the inputs inside expand.grid function and 
#' range_lin_afterComp and range_log_afterComp correspond to the real number of classes used to create the joint distribution.
#' Usally, the number of classes don't change. If too large or too small value of alpha in log_breaks function is given, 
#' it can change.}
#' }
#' 
#' @export
#'
#' @importFrom doParallel registerDoParallel
#' @importFrom foreach foreach %dopar%
#' @importFrom stringr str_c str_split
#' @importFrom parallel detectCores
#' @examples
#' #\donttest{
#' #joint.distrib.2v.LinLog(matrix(rnorm(1000,52,15),ncol=10),
#' #matrix(rpois(1000,50),ncol=10),range_lin=seq(5,10,1), 
#' #range_log=seq(5,15,2),range_alpha_=round(exp(seq(-9,0,length.out = 5)),4))
#' #}


joint.distrib.2v.LinLog <- function(matrix_lin, 
                                    matrix_log, 
                                    range_lin=seq(15,30,1), 
                                    range_log=seq(10,30,2), 
                                    range_alpha_=round(exp(seq(-9,0,length.out = 10)),4),
                                    round_LogBreaks =0,
                                    DIGLAB_lin=3,
                                    N.cores=parallel::detectCores()-1,
                                    effect.threshold.begin=NA,
                                    effect.threshold.end=NA,
                                    centering_status=FALSE,
                                    scaling_status=FALSE)
  
  {
  
  registerDoParallel(cores=N.cores)
  
  vector_lin <- c(matrix_lin)
  vector_log <- c(matrix_log)
  
  ## creation of candidate joint distribution
  range_dframe <- expand.grid(range_lin,range_log,range_alpha_)
  names(range_dframe) <- c("ntpr_appit","nrdc_appit","alpha_")
  x_appit <- new.env()
  
  ntpr_appit_ch<-NULL
  nrdc_appit_ch<-NULL
  alpha_ch<-NULL
  
  x_appit <- foreach(ntpr_appit_ch =range_dframe$ntpr_appit,
                      nrdc_appit_ch =range_dframe$nrdc_appit,
                      alpha_ch      =range_dframe$alpha_
  ) %dopar% {
    
    ## creation of bins (linear scale)
    cl_tpr<-create_classesLFV(to_class=vector_lin,
                              change_step=c(floor(min(vector_lin)),
                                            ceiling(max(vector_lin))),
                              amplitude=diff( c(floor(min(vector_lin)),
                                                ceiling(max(vector_lin))) )/ ntpr_appit_ch
                              )
    
    # creation of bins (logarithmic scale)
    cl_rdc<-log_breaks(alpha=alpha_ch, xmin=min(vector_log), xmax=ceiling(max(vector_log)/10)*10,
                    J=nrdc_appit_ch, round_breaks = round_LogBreaks, plot_breaks = FALSE,
                    effect.threshold.begin=effect.threshold.begin, 
                    effect.threshold.end=effect.threshold.end)
    
    ## Histogram 2D 
    # histogram for each individual (loop for)
    ht_ind<-array(NA,dim = c(ncol(matrix_lin), cl_rdc$n.ClassesL, cl_tpr$N.ClassesL),
                  dimnames=list(colnames(matrix_lin), cl_rdc$ClassesL, cl_tpr$ClassesLFV))
    
    for(i in c(1:ncol(matrix_lin)) ){
      ht_ind[i,,]<-HIST_2D(x=matrix_log[,i],y=matrix_lin[,i],breaks_x=cl_rdc$LimitsL,
                           breaks_y=cl_tpr$Breaks_LFV,DIGLAB_Y=DIGLAB_lin)$Hist.Values
    }
    
    # Transformation of the contingency table into a matrix (n_ind x (nclasses_tpr x nclasses_rdc)).  
    n_nvvar<-tidyr::spread_(as.data.frame.table(ht_ind), "Var1", "Freq")
    rownames(n_nvvar)<-str_c(n_nvvar$Var2,"_",n_nvvar$Var3)
    assign(paste0(ntpr_appit_ch,"_",nrdc_appit_ch,"_",alpha_ch),
           base::scale(t(n_nvvar[,c(3:ncol(n_nvvar))]),center = centering_status, scale = scaling_status),
           envir = x_appit) 
  }
  
  nc_aftercomputation<-t(
    simplify2array(
      lapply(
        x_appit, 
        function(matcand){
          MatClassparmat= str_split(colnames( matcand ),"_",simplify = T)
          nc_log=length(unique(MatClassparmat[,1]))
          nc_lin=length(unique(MatClassparmat[,2]))
          to_ret=c(nc_lin,nc_log) 
          names(to_ret)= c("nc_lin","nc_log")
          return(to_ret)
        }
      )
    )
  )
  range_dframe <- cbind(range_dframe,nc_aftercomputation[,"nc_lin"],nc_aftercomputation[,"nc_log"])
  names(range_dframe) <- c("range_lin","range_log","range_alpha_","range_lin_afterComp","range_log_afterComp")
  return(list(x_appit, range_dframe))
}
  
# joint.distrib.2v.LinLog(matrix(rnorm(100),ncol=10, 
#                                dimnames = list(1:10, 1:10)),matrix(rpois(100,5),ncol=10) )

  
  