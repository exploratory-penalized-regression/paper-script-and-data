# Supplementary material for SpiceFP approach: An exploratory penalized regression to identify combined effects of temporal variables - Application to agri-environmental issues

**Authors: Girault Gnanguenon Guesse, Patrice Loisel, Bénedicte Fontez, Thierry Simonneau, Nadine Hilgert**


# File composition

- README.R               : main file. Contains an application of SpiceFP approach.

- SpiceFP materials.Rproj : the project to open with Rstudio

- Data4spiceFP.RData      : data. rdc_if1_cible and tpr_if1_cible contains 2 temporal predictors (irradiance, temperature) obseved from sunrise to 12H a.m. during 7 days, Y_nc contains Ferari index difference during 7 days (24H*7=168H).

- supmaterials.RData      : running the example takes about 1.3 hours with 4 cores. supmaterials.RData can be directly loaded and thus you can go directly to the result visualization from line 80.

- R file                  : contains the functions that will be run for the implementation of the approach and the visualization of the results. Each function contains its description in its header. 

- addins                  : contains the datasets (beta coefficients and X matrix) used for the simulations


**Objective of the use case** : find the irradiance-temperature combinations that influence the variation of the Ferari index in the morning (sunrise to 12H a.m.).

               
## Packages
```{r}
library(tidyverse)
library(gridExtra)
library(genlasso)
library(doParallel)
library(hms)
library(ggrepel)
```


## Functions
```{r}
source("R/ParametersFrame_genlasso2.R")
source("R/function_HIST_2D.R")
source("R/function_joint.distrib.2v.LinLog.R")
source("R/function_ParametersFrame_genlasso.R")
source("R/function_rdc_logbreaks.R")
source("R/function_scale_raster.R")
source("R/function_Xu_to_SumX_NA.R")
source("R/function_spicefp.lin.log.R")
source("R/function_create_classesLFV.R")
```

## Data
Load data from Data4spiceFP.RData file 

```{r}
load("Data4spiceFP.RData")
```

## Temporal Predictors

- Irradiance
```{r}
m_rdc_if1_cible<-as.matrix(Irradiance[,-c(1)])
c_rdc_if1_cible<-c(m_rdc_if1_cible)
```

- Temperature
```{r}
m_tpr_if1_cible<-as.matrix(Temperature[,-c(1)])
c_tpr_if1_cible<-c(m_tpr_if1_cible)
```

- Response variable (centered)
```{r}
Y<-FerariIndex_Difference$fi_dif-mean(FerariIndex_Difference$fi_dif)
names(Y) <- rownames(FerariIndex_Difference)
```

## Implementation of the SpiceFP approach for two explanatory temporal variables: one to be partitioned according to a linear scale (temperature) and another to be partitioned according to a logarithmic scale (irradiance)

**Remark** : The implementation of this example requires approximately 1.348865 hours with 2.80GHz × 4 cores (Intel Core i7-7600U CPU). If you don't want to run it, you can load file supmaterials.RData (load("supmaterials.RData")) and continue the running

Requires approximately 1.348865 hours with 2.80GHz × 4 cores. The parameterization of this example does not rake wide, just to save execution time. In fact,it is important to rake wide when you don't have expert knowledge. 

```{r}
start_time_genlasso <- Sys.time()
res_test1 <- spicefp.lin.log(
  max_iter_=1,
  Criteria ="aic_par",
  matrix_lin   = m_tpr_if1_cible,
  matrix_log   = m_rdc_if1_cible,
  range_lin    = 10:15, 
  range_log    = seq(20,25,2),
  range_alpha_ = c(0.01,0.02,0.03),
  effect.threshold.begin = NA,
  effect.threshold.end = NA,
  y=Y,
  Lambda_length=20,
  Gamma_length=3,
  round_beta_gflx=9,
  r_GAMMA = as.list(c(0.001,0.1,10)), 
  N_cores_jd=parallel::detectCores(),
  N_cores_pfg=parallel::detectCores(),
  nBestModels = 1000, 
  nBestMatGam = 30,
  nFineTuneGamma = 5,
  nFineTuneLambda = 20,
  Centering_status=TRUE ## centers each new explanatory variable
)
duration_genlasso <- Sys.time() - start_time_genlasso
# duration_genlasso
# End 
# supmaterials.RData 
# save.image("supmaterials.RData") 
  
```

**Get matrix SumXNA ( to identify ) and coefficients related to Xu to avoid saturating the RAM memory, the coefficients obtained at each iteration are not retained. Only the input parameters of the approach and the quality parameters of the built model are kept.**

## Results

- X associés

```{r}
iterOfInterest<-res_test1[[2]][[1]]
Xu_Opt= iterOfInterest$X_elected
idx_Xu_Opt=iterOfInterest$idx_elected_Criteria
param_elected_model= iterOfInterest$res_ParametersFrame_genlasso_fine[idx_Xu_Opt,]
nrow_Xu_Opt = param_elected_model["range_lin_afterComp"]
ncol_Xu_Opt = param_elected_model["range_log_afterComp"]
ResXu_to_SumX_NA<-Xu_to_SumX_NA(Xu=Xu_Opt, nrow_Xu=nrow_Xu_Opt, ncol_Xu=ncol_Xu_Opt)
```

- Scaling for interaction with other selected matrices

```{r}
beta1_<-c(res_test1[[2]][[1]]$Coef_X_elected$beta)
names(beta1_)<-rownames(res_test1[[2]][[1]]$Coef_X_elected$beta)
beta1_[which(names(beta1_) %in% ResXu_to_SumX_NA$pairs_tobe_na)] = NA 
Xsim_sc <- scale_raster( beta1_ )$scaled.matrix

Xsim_sc_plot <- as.data.frame.table(Xsim_sc)
Xsim_sc_plot$Var1 <- as.numeric(as.character(Xsim_sc_plot$Var1))
Xsim_sc_plot$Var2 <- as.numeric(as.character(Xsim_sc_plot$Var2))
```

- Visualization of the coefficients selected

```{r}
p_beta_ <-ggplot(Xsim_sc_plot, aes(x = Var2 , y = Var1)) + geom_raster(aes(fill=Freq)) +
  labs(title = "Coefficients", subtitle="Iteration 1",x="Irradiance (mmol/m²/s - Logarithmic scale)",
       y="Temperature (°C)",fill = " Coef") + 
  theme(plot.title = element_text(size=20,hjust = 0.5),axis.text.x = element_text(vjust=1, size = 8, hjust = 1),
        plot.subtitle = element_text(size=16,hjust = 0.5))+
  scale_fill_gradient2(low = "blue",high="red",mid="white", midpoint=0,na.value="black",limits=c(-0.05,0.05),
                       breaks =seq(-0.05,0.05,0.025)) +
  scale_x_continuous( breaks = seq(min(Xsim_sc_plot$Var2, na.rm = T)-0.02, 
                                   max(Xsim_sc_plot$Var2, na.rm = T)+ 0.02,length.out = 10), 
                      label = as.character(round(exp(seq(min(Xsim_sc_plot$Var2, na.rm = T)-0.02,
                                                         max(Xsim_sc_plot$Var2, na.rm = T)+0.02,length.out = 10)),0)),
                      limits=range(seq(min(Xsim_sc_plot$Var2, na.rm = T)-0.02,max(Xsim_sc_plot$Var2, na.rm = T)+0.02,length.out = 10))) +
  scale_y_continuous(breaks = seq(15,50,5), limits=range(Xsim_sc_plot$Var1)+c(-0.1,0.1)) + 
  theme(axis.text.x = element_text(size = 10,face="bold", colour = "black"), 
        axis.text.y = element_text(size = 10,face="bold", colour = "black"), 
        axis.title.x =element_text(size = 13,face="bold", colour = "black"), 
        axis.title.y =element_text(size = 13,face="bold", colour = "black"), 
        legend.text =element_text(size = 10,face="bold", colour = "black"), 
        legend.title = element_text(size = 10,face="bold", colour = "black")) + 
  theme(panel.background=element_rect(fill="transparent",colour=NA), 
        panel.grid.major = element_line(colour = "white"))
p_beta_ 
```


**Remark : SpiceFP is programmed to do more iterations on request (option max_iter) using residuals as new explanative variable**

# Quality of fit 
## Data for ggplot
```{r}
ydata12 <- data.frame(y=Y, y_hat=res_test1[[2]][[1]]$X_elected %*% res_test1[[2]][[1]]$Coef_X_elected$beta) 
names(ydata12) <- c("y_obs", "y_hat")
```

## Visualization
```{r}
p_yvsyhat12 <- ggplot(data = ydata12, aes(y = y_hat , x = y_obs)) + geom_point(size=2) + 
  geom_smooth(method='lm',formula=y~x,size=2,level=0) + geom_abline(mapping=aes(slope=1, intercept=0),
                                                                    colour="red", linetype= 2, size=2) + 
  labs(y =bquote(hat("Y")), x = "Y", 
       subtitle = paste("slope = ",round(lm(ydata12$y_hat~ydata12$y_obs)$coefficients[2],3)), 
       title="Quality of fit") + theme(plot.title = element_text(size=20,hjust = 0.5),
                                       axis.text.x = element_text(vjust=1,size = 8, hjust = 1),
                                       plot.subtitle = element_text(size=16,hjust = 0.5))+
  theme(axis.text.x = element_text(size = 10,face="bold", colour = "black"), 
        axis.text.y = element_text(size = 10,face="bold", colour = "black"), 
        axis.title.x =element_text(size = 13,face="bold", colour = "black"), 
        axis.title.y =element_text(size = 13,face="bold", colour = "black"), 
        legend.text =element_text(size = 10,face="bold", colour = "black"), 
        legend.title = element_text(size = 10,face="bold", colour = "black") )+
  theme(panel.background=element_rect(fill="white", colour = "white")) + 
  scale_x_continuous(limits=c(-1,1)) + scale_y_continuous(limits=c(-1,1))
p_yvsyhat12
```

# Residuals histogram

```{r}
err12 <- Y-(res_test1[[2]][[1]]$X_elected %*% res_test1[[2]][[1]]$Coef_X_elected$beta)
```

## Normality of residuals
```{r}
shapiro.test(err12)
```

## Visualisation
```{r}
p_err12<-ggplot(data=data.frame(err12), aes(err12)) +  geom_histogram(bins =20,color="black", fill="white") +  
  labs(x="Residuals",y="Frequency",title = "Residuals histogram") + 
  theme(plot.title = element_text(size=20,hjust = 0.5),
        axis.text.x = element_text(vjust=1, hjust = 1,size = 10,face="bold", colour = "black"),
        plot.subtitle = element_text(size=16,hjust = 0.5))+
  theme(axis.text.y = element_text(size = 10,face="bold", colour = "black"), 
        axis.title.x =element_text(size = 13,face="bold", colour = "black"), 
        axis.title.y =element_text(size = 13,face="bold", colour = "black"), 
        legend.text =element_text(size = 10,face="bold", colour = "black"), 
        legend.title = element_text(size = 10,face="bold", colour = "black"))+
  theme(panel.background=element_rect(fill="white", colour = "white"))+ 
  scale_x_continuous(limits=c(-1,1)) + 
  scale_y_continuous(limits=c(0,10))
p_err12
```
