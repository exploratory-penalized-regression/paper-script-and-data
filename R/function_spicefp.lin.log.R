
# spicefp.lin.log: Implementation of the SpiceFP approach for two explanatory functional variables: one to 
#   be partitioned according to a linear scale (temperature) and another to be partitioned 
#   according to a logarithmic scale (irradiance)

##  Function spicefp.lin.log
spicefp.lin.log <- function(
  max_iter_,
  Criteria ,
  matrix_lin,
  matrix_log,
  range_lin,
  range_log,
  range_alpha_,
  effect.threshold.begin,
  effect.threshold.end= NA,
  y,
  Lambda_length,
  Gamma_length,
  round_beta_gflx,
  r_GAMMA,
  N_cores_jd=parallel::detectCores()-1,
  N_cores_pfg=parallel::detectCores()-1,
  nBestModels,
  nBestMatGam,
  nFineTuneGamma,
  nFineTuneLambda,
  Centering_status=TRUE 
){
  
  # Creation of candidate matrices Xu
  dstrbt_cnjnt_eff <- joint.distrib.2v.LinLog(matrix_lin=matrix_lin   , 
                                              matrix_log=matrix_log   ,
                                              range_lin=range_lin    ,
                                              range_log=range_log    ,
                                              range_alpha_=range_alpha_ ,
                                              effect.threshold.begin=effect.threshold.begin,
                                              effect.threshold.end = effect.threshold.end,
                                              N.cores=N_cores_jd,
                                              centering_status = Centering_status,
  )
  
  # Preparing the arguments of the ParametersFrame_genlasso function
  all_candidates=dstrbt_cnjnt_eff
  all_n_c=dstrbt_cnjnt_eff[[2]]$range_log_afterComp
  all_n_r=dstrbt_cnjnt_eff[[2]]$range_lin_afterComp
  all_alpha_log=dstrbt_cnjnt_eff[[2]]$range_alpha_
  
  tokeep <-list()
  iter_=1
  check_coef_zeros <- FALSE
  while(iter_ <= max_iter_ & check_coef_zeros==FALSE){ # Shutdown conditions
    # Criteria indexed by the parameter combination n_r, n_c, alpha_log, Lambda, Gamma 
    restt<-ParametersFrame_genlasso(y=y, all_candidates=all_candidates, 
                                    all_n_c=all_n_c, 
                                    all_n_r=all_n_r,
                                    all_alpha_log=all_alpha_log,
                                    Lambda_length=Lambda_length,
                                    Gamma_length=Gamma_length,
                                    round_beta_gflx=round_beta_gflx,
                                    r_GAMMA=r_GAMMA,
                                    N.cores=N_cores_pfg)
    
    restt_OrderByCrit <-restt[order(restt[,Criteria], decreasing = FALSE)[1:nBestModels],]
    uniqMat_OrderByCrit <-unique(restt_OrderByCrit[,c("range_lin_","range_log_",
                                                      "range_lin_afterComp",
                                                      "range_log_afterComp",
                                                      "alpha_log","Gamma")])
    uniqMat_OrderByCrit_BestMatGam <- data.frame(uniqMat_OrderByCrit[1:min(nrow(uniqMat_OrderByCrit), nBestMatGam),])
    nBestMatGam_real <- nrow(uniqMat_OrderByCrit_BestMatGam)
    
    # Find matrices conditioned by all matrix creation parameters uniqMat_OrderByCrit_BestMatGam
    topMat_param<-uniqMat_OrderByCrit_BestMatGam[,c("range_lin_","range_log_","alpha_log",
                                                    "range_lin_afterComp","range_log_afterComp")]
    names(topMat_param)<-c("range_lin","range_log","range_alpha_",
                           "range_lin_afterComp","range_log_afterComp")
    topMat_param_vect <- do.call("paste", c(topMat_param[, , drop = FALSE], sep = "_"))
    param_crea_vect<-do.call("paste", c(dstrbt_cnjnt_eff[[2]][, , drop = FALSE], sep = "_"))
    topMat_param_vect_IN_param_crea_vect <- match(topMat_param_vect, param_crea_vect)
    
    #------------------------------------------------------------
    # A function to use in order to obtain a log scale sequence
    seq_log <- function(n,a,c){return(
      a*(c/a)^((seq(2:n-1)-1)/(n-1))
    )}
    # seq_log(n=10,a=5,c=10)
    # diff(log(seq_log(10,5,10)))
    #------------------------------------------------------------
    
    # Fine-tuned gamma range
    GammaToFine <- uniqMat_OrderByCrit_BestMatGam$Gamma
    c_r_GAMMA<- unlist(r_GAMMA)[order(unlist(r_GAMMA),decreasing = F)]
    GammaToFine <- map(as.list(GammaToFine), function(c_r_GAMMAinteger){
      GammaToFine_bin_iter <- c(max(min(c_r_GAMMA, na.rm = T), c_r_GAMMA[which(c_r_GAMMA==c_r_GAMMAinteger)-1], na.rm = T),
                                min(max(c_r_GAMMA, na.rm = T), c_r_GAMMA[which(c_r_GAMMA==c_r_GAMMAinteger)+1], na.rm = T))
      GammaToFine_value_iter <- seq_log(n=nFineTuneGamma+2,a=GammaToFine_bin_iter[1],c=GammaToFine_bin_iter[2])[-1]
      nFineTuneGamma_real <- length(GammaToFine_value_iter)
      return(list(GammaToFine_bin=GammaToFine_bin_iter, GammaToFine_value=GammaToFine_value_iter, nFineTuneGamma_real=nFineTuneGamma_real))
    })
    
    ## Build the all_candidates_ input for refining
    Mat1_top <- dstrbt_cnjnt_eff[[1]][rep(topMat_param_vect_IN_param_crea_vect,
                                          map_dbl(1:length(topMat_param_vect_IN_param_crea_vect),
                                                  function(ll){return(GammaToFine[[ll]]$nFineTuneGamma_real)})
    )]
    top_param<-topMat_param[rep(1:nrow(topMat_param), 
                                map_dbl(1:nrow(topMat_param), function(yy){return(GammaToFine[[yy]]$nFineTuneGamma_real)})
    ),]  ;  rownames(top_param)<-NULL
    top_param$range_gamma <- unlist(map(1:nBestMatGam_real, function(zz){return(GammaToFine[[zz]]$GammaToFine_value)}))
    all_candidates_ <-list(Mat1_top,top_param)
    
    A2 <- ParametersFrame_genlasso2(y=y, all_candidates=all_candidates_, 
                                    Lambda_length=nFineTuneLambda, round_beta_gflx=round_beta_gflx)
    
    ## Identify the partition selected at the iteration under consideration
    idx_elu_GenAic <- which.min(A2[,Criteria])
    elu_GenAic <- as.list(A2[idx_elu_GenAic,])
    
    ## Matrix for each individual
    X_inds_GenAic <- joint.distrib.2v.LinLog(matrix_lin   = m_tpr_if1_cible, 
                                             matrix_log   = m_rdc_if1_cible,
                                             range_lin    = elu_GenAic$range_lin_,
                                             range_log    = elu_GenAic$range_log_,
                                             range_alpha_ = elu_GenAic$alpha_log,
                                             effect.threshold.begin = effect.threshold.begin,
                                             effect.threshold.end = effect.threshold.end,
                                             centering_status = Centering_status)[[1]][[1]]
    ## Coefficients associated to the X_inds_GenAic matrix with genlasso library
    
    GFL_GenAic = fusedlasso2d(y,X_inds_GenAic,dim1=elu_GenAic$range_lin_afterComp, 
                              dim2=elu_GenAic$range_log_afterComp, 
                              gamma = elu_GenAic$Gamma)
    Coef_GenAic <- coef.genlasso(GFL_GenAic, lambda=elu_GenAic$Lambda)
    rownames(Coef_GenAic$beta) <- colnames(X_inds_GenAic)
    ## Quick Check
    Verif_df <- Coef_GenAic$df == elu_GenAic$df_est
    Verif_lam<- Coef_GenAic$lambda == elu_GenAic$Lambda
    
    ## Residuals as a New Response Variable
    xbeta <- X_inds_GenAic%*%Coef_GenAic$beta
    y_residuals <- y-xbeta
    tokeep[[iter_]] <- list(y_used=y, res_ParametersFrame_genlasso= restt,
                            res_ParametersFrame_genlasso_fine = A2,
                            X_fine = all_candidates_,
                            idx_elected_Criteria=idx_elu_GenAic, elected_parameters=elu_GenAic,
                            X_elected=X_inds_GenAic, fusedlasso2d_X_elected=GFL_GenAic,
                            Coef_X_elected=Coef_GenAic,Verif_df=Verif_df,Verif_lam=Verif_lam,
                            X_electedBeta=xbeta, y_residuals=y_residuals)
    y<-y_residuals
    
    if(isTRUE(all.equal(c(Coef_GenAic$beta), rep(0,nrow( Coef_GenAic$beta )))))
      print(paste("Best coefficients chosen by",Criteria,"at iteration",iter_,"are all equal to 0.",
                  "Stop after iteration",iter_))
    if(isTRUE(all.equal(c(Coef_GenAic$beta), rep(0,nrow( Coef_GenAic$beta )))))  check_coef_zeros=T 
    iter_=iter_+1
  }
  return(list(X=dstrbt_cnjnt_eff, ResultsByIter=tokeep))
}